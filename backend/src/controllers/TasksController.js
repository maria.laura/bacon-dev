const express = require('express');

const Tasks = require('../models/Tasks');

module.exports = {

    /* criar tarefa */
    async create(request, response) {
        const { task } = request.body;
        try {
            await Tasks.create({
                task
            });
            return response.status(202).send({ ok: 'Requisição bem sucedida'  })
        } catch (err) {
            return response.status(400).send({ error: 'Erro! Tente novamente!' })
        }
    },

    /* listar tarefa */
    async list(request, response) {
        try {
            const tarefa = await Tasks.find();
            return response.json(tarefa);
        } catch (err) {
            return response.status(400).send({ error: 'Erro ao carregar tarefa.' })
        }
    },

    /* excluir tarefa */
    async deletar(request, response) {
        const idTask = request.params.id;
        try {
            const verificaTarefa = await Tasks.findOne({ _id: idTask });
            if(!verificaTarefa) return response.status(404).send({ error: 'Tarefa não encontrada' });
            await Tasks.deleteOne({ _id: idTask });
            return response.status(200).send({ ok: 'Requisição bem sucedida' });
        } catch (err) {
            return response.status(400).send({ error: 'Me desculpe, algo deu errado' });
        }    
    }
}