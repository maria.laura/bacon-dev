const mongoose = require('mongoose');

const Task= new mongoose.Schema({
    task: {
        type: String,
        required: true,
        maxlength: 25,
        minlength: 5,
    }
});

module.exports = mongoose.model('Tasks', Task);