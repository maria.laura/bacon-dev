
const { Router } = require('express');

const TasksController = require('./controllers/TasksController');


const routes = Router();


routes.post('/tasks', TasksController.create);

routes.get('/tasks', TasksController.list);

routes.delete('/tasks/:id', TasksController.deletar);

module.exports = routes;